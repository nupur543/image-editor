//
//  ImageContainer.swift
//  ImageEditor
//
//  Created by Nupur Sharma on 28/01/19.
//  Copyright © 2019 Virtual Employee. All rights reserved.
//

import UIKit

class ImageContainer: NSObject {

    var originalImage: UIImage!
    var modifiedImage: UIImage!
    var thumbImage: UIImage!
    var imageOrientation: CGFloat!
    
    /// - Parameter image: <#image description#>
    func prepare(_ image: UIImage?) {
        self.originalImage = image
        
        self.modifiedImage = image
        self.imageOrientation = 0.0
        if let image2 = image{
            self.thumbImage = thumbFromImage(image2).toUIImage() // create thumb image, only in size and ratio
        }else{
            self.thumbImage = nil
        }
    }
    
    var thumbModifiedImage: UIImage{
        
        return thumbFromImage(self.modifiedImage).toUIImage()
    }
    
    static let shared = ImageContainer()
    private override init() {
        super.init()

    }
    
    fileprivate func thumbFromImage(_ img: UIImage) -> CIImage {
        let k = img.size.width / img.size.height
        let scale = UIScreen.main.scale
        let thumbnailHeight: CGFloat = 300 * scale
        let thumbnailWidth = thumbnailHeight * k
        let thumbnailSize = CGSize(width: thumbnailWidth, height: thumbnailHeight)
        UIGraphicsBeginImageContext(thumbnailSize)
        img.draw(in: CGRect(x: 0, y: 0, width: thumbnailSize.width, height: thumbnailSize.height))
        let smallImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return smallImage!.toCIImage()!
    }
}
