//
//  ImgView+Extension.swift
//  ImageEditor
//
//  Created by Nupur Sharma on 21/01/19.
//  Copyright © 2019 Virtual Employee. All rights reserved.
//

import UIKit

internal extension UIImage {

    func toCIImage() -> CIImage? {
        return self.ciImage ?? CIImage(cgImage: self.cgImage!)
    }
    
    func cropImage(toRect: CGRect) -> UIImage? {
        let cgImage :CGImage! = self.cgImage
        let croppedCGImage: CGImage! = cgImage.cropping(to: toRect)
        
        return UIImage(cgImage: croppedCGImage)
    }
    
    func rotate(radians: CGFloat) -> UIImage {
        
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, true, self.scale)
        let context = UIGraphicsGetCurrentContext()!
        
        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
            
        return newImage ?? self
    }
    
    func fixOrientation() -> UIImage {
            
            // No-op if the orientation is already correct
        if ( self.imageOrientation == UIImage.Orientation.up ) {
                return self;
            }
            
            // We need to calculate the proper transformation to make the image upright.
            // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
            var transform: CGAffineTransform = CGAffineTransform.identity
            
        if ( self.imageOrientation == UIImage.Orientation.down || self.imageOrientation == UIImage.Orientation.downMirrored ) {
                transform = transform.translatedBy(x: self.size.width, y: self.size.height)
                transform = transform.rotated(by: CGFloat(Double.pi))
            }
            
        if ( self.imageOrientation == UIImage.Orientation.left || self.imageOrientation == UIImage.Orientation.leftMirrored ) {
                transform = transform.translatedBy(x: self.size.width, y: 0)
                transform = transform.rotated(by: CGFloat(Double.pi / 2.0))
            }
            
        if ( self.imageOrientation == UIImage.Orientation.right || self.imageOrientation == UIImage.Orientation.rightMirrored ) {
                transform = transform.translatedBy(x: 0, y: self.size.height);
                transform = transform.rotated(by: CGFloat(-Double.pi / 2.0));
            }
            
        if ( self.imageOrientation == UIImage.Orientation.upMirrored || self.imageOrientation == UIImage.Orientation.downMirrored ) {
                transform = transform.translatedBy(x: self.size.width, y: 0)
                transform = transform.scaledBy(x: -1, y: 1)
            }
            
        if ( self.imageOrientation == UIImage.Orientation.leftMirrored || self.imageOrientation == UIImage.Orientation.rightMirrored ) {
                transform = transform.translatedBy(x: self.size.height, y: 0);
                transform = transform.scaledBy(x: -1, y: 1);
            }
            
            // Now we draw the underlying CGImage into a new context, applying the transform
            // calculated above.
            let ctx: CGContext = CGContext(data: nil, width: Int(self.size.width), height: Int(self.size.height),
                                           bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0,
                                           space: self.cgImage!.colorSpace!,
                                           bitmapInfo: self.cgImage!.bitmapInfo.rawValue)!;
            
            ctx.concatenate(transform)
            
        if ( self.imageOrientation == UIImage.Orientation.left ||
                self.imageOrientation == UIImage.Orientation.leftMirrored ||
                self.imageOrientation == UIImage.Orientation.right ||
                self.imageOrientation == UIImage.Orientation.rightMirrored ) {
                ctx.draw(self.cgImage!, in: CGRect(x: 0,y: 0,width: self.size.height,height: self.size.width))
            } else {
                ctx.draw(self.cgImage!, in: CGRect(x: 0,y: 0,width: self.size.width,height: self.size.height))
            }
            
            // And now we just create a new UIImage from the drawing context and return it
            return UIImage(cgImage: ctx.makeImage()!)
        }
    
     func rotatedImage(image: UIImage?, byDegress degress: CGFloat, backroundColor: UIColor? = nil) -> UIImage? {
        guard let image = image else {
            return nil
        }
    
        let angle = degress
        
        let imageView = UIImageView(image: image)
        
        let transform = CGAffineTransform(rotationAngle: angle)
        
        let transformedSize: (CGSize) = {
            let leftFromCenter = CGPoint(x: -imageView.frame.size.width*0.5, y: imageView.frame.size.height*0.5)
            let rightFromCenter = CGPoint(x: imageView.frame.size.width*0.5, y: imageView.frame.size.height*0.5)
            
            let leftTransformed = leftFromCenter.applying(transform)
            let rightTransformed = rightFromCenter.applying(transform)
            
            return CGSize(width: max(abs(leftTransformed.x*2.0), abs(rightTransformed.x*2.0)), height: max(abs(leftTransformed.y*2.0), abs(rightTransformed.y*2.0)))
        }()
        
        let canvasView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: transformedSize.width, height: transformedSize.height))
        canvasView.backgroundColor = backroundColor
        canvasView.addSubview(imageView)
        imageView.center = CGPoint(x: transformedSize.width*0.5, y: transformedSize.height*0.5)
        imageView.transform = transform
        
        return UIImage.viewSnapshot(view: canvasView)
    }
    
    private static func viewSnapshot(view: UIView?) -> UIImage? {
        guard let view = view else {
            return nil
        }
       
        UIGraphicsBeginImageContext(view.bounds.size)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }

        view.layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
        
//        UIGraphicsBeginImageContextWithOptions(size, true, scale)
//        let context = UIGraphicsGetCurrentContext()
//        context?.translateBy(x: (size.width) / 2.0, y: size.height / 2.0)
//        context?.concatenate(transform)
//        context?.translateBy(x: size.width / -2.0, y: size.height / -2.0)
//        draw(in: CGRect(x: 0.0, y: 0.0, width: size.width , height: size.height))
//        let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()//common
//        UIGraphicsEndImageContext()//common
//        return rotatedImage!
    }
    
    
}

internal extension CIImage {
    func toUIImage() -> UIImage {
        /* If need to reduce the process time, than use next code. But ot produce a bug with wrong filling in the simulator.
         return UIImage(ciImage: self)
         */
        let context: CIContext = CIContext.init(options: nil)
        let cgImage: CGImage = context.createCGImage(self, from: self.extent)!
        let image: UIImage = UIImage(cgImage: cgImage)
        return image
    }
    
    func toCGImage() -> CGImage? {
        let context = CIContext(options: nil)
        if let cgImage = context.createCGImage(self, from: self.extent) {
            return cgImage
        }
        return nil
    }
}
