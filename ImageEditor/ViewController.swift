//
//  ViewController.swift
//  ImageEditor
//  Created by Nupur Sharma on 21/01/19.
//  Copyright © 2019 Virtual Employee. All rights reserved.

import UIKit
import Photos

class ViewController: UIViewController {

    @IBOutlet weak var btnPick: UIButton!
    @IBOutlet weak var selectedImg: UIImageView!
    
    let imagePicker    = UIImagePickerController()
    var pickedimage    : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        imagePicker.modalPresentationStyle = .overCurrentContext
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
        if pickedimage != nil{
            ImageContainer.shared.prepare(pickedimage)
        }
        
    }
   
    
    @IBAction func pickPressed(_ sender: Any) {
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func editPressed(_ sender: Any) {
        
        if let _ = self.pickedimage{
            self.performSegue(withIdentifier: "editSegue", sender: self)
        }else{
            btnPick.setTitle("No Image", for: .normal)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editSegue"{
            
            let destinatn = segue.destination as! MyTabViewController
            
        }
    }
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            
            pickedimage       = pickedImage
            selectedImg.image = pickedImage
            
            ImageContainer.shared.prepare(pickedImage)
           
        }

        imagePicker.dismiss(animated: true, completion: nil)
    }
    
}
