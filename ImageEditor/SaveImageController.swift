import UIKit

class SaveImageController: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var imgView: UIImageView!
    
    var inputPhoto: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_back"),
                                                                        style: .plain,
                                                                        target: self,
                                                                        action: #selector(backPressed))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(savePressed))
        
        inputPhoto = ImageContainer.shared.modifiedImage
      
        if let deg = ImageContainer.shared.imageOrientation{
           
            imgView.image = inputPhoto
            let image = ImageContainer.shared.modifiedImage
            self.imgView.image = image
            
           let angle = (deg) * 2 * .pi
            //let radians = CGFloat(__sinpi((angle.native)/180.0))
            imgView.image = inputPhoto.rotatedImage(image: imgView.image, byDegress: angle / 90, backroundColor: .clear)
            
        }else{
            imgView.image = inputPhoto
        }
        
       
    }
    
    @objc func backPressed(){
        let ac = UIAlertController(title: "Cancel editing?", message: "", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (actn) in
            self.navigationController?.popToRootViewController(animated: true)
            
        }))
        ac.addAction(UIAlertAction(title: "No", style: .default, handler: { (actn) in
            
        }))
        
        present(ac, animated: true)
        
    }
    
    @objc func savePressed(){
        UIImageWriteToSavedPhotosAlbum(inputPhoto, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save Error!", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: { (actn) in
                self.navigationController?.popToRootViewController(animated: true)
                
            }))
            present(ac, animated: true)
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
