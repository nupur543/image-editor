//  CropViewController.swift
//  ImageEditor
//  Created by Nupur Sharma on 31/01/19.
//  Copyright © 2019 Virtual Employee. All rights reserved.

import UIKit
//import AKImageCropperView

class CropViewController: UIViewController {

    @IBOutlet weak var btnCrop: UIButton!
    @IBOutlet weak var cropView: UIView!
   
    @IBOutlet var cropLabels: [UIButton]!
    @IBOutlet weak var btnFreeCrop: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var inputPhoto: UIImage!
    var controller : CropManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.setupBarButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        loader.startAnimating()
        inputPhoto = ImageContainer.shared.modifiedImage
        
        controller = CropManager()
        controller.image = inputPhoto
        controller.resetCropRect()

        controller.view.frame = self.cropView.bounds;
        controller.willMove(toParent: self)
        controller.rotationEnabled = false
        self.cropView.addSubview(controller.view)
        self.addChild(controller)
        controller.didMove(toParent: self)
        
        let angle = (ImageContainer.shared.imageOrientation) * 2 * .pi
        controller.image = inputPhoto.rotatedImage(image: inputPhoto, byDegress: angle / 90, backroundColor: .clear)
        self.cropFreePressed(btnFreeCrop)
        
    }
    
    // MARK: Setup - ⚙️
    fileprivate func setupBarButton() {
        
        let undo = UIBarButtonItem(title: "Reset", style: .done, target: self, action: #selector(undoPressed))
        
        self.parent?.navigationItem.leftBarButtonItems = [UIBarButtonItem(image: UIImage(named: "ic_back"),
                                                                          style: .plain,
                                                                          target: self,
                                                                          action: #selector(backPressed)), undo]
        
        self.parent?.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_tick"),
                                                                         style: .done,
                                                                         target: self,
                                                                         action: #selector(savePressed))
    }
    
    @objc
    func undoPressed() {
        controller.image = inputPhoto
        self.cropFreePressed(btnFreeCrop)
        ImageContainer.shared.modifiedImage = inputPhoto
    }
    
    @objc func backPressed(){
        self.navigationController?.popViewController(animated: true)
    }
    
     @objc func savePressed(){
      
        self.performSegue(withIdentifier: "showEdittedSegue", sender: self)
    }
    
    @IBAction func crop1Pressed(_ sender: UIButton) {
     
        controller.resetCropRect()
        controller.cropAspectRatio = 1 / 1
        controller.isExpandable = false
        
        cropLabels.forEach{$0.backgroundColor = .clear
            $0.setTitleColor(.white, for: .normal)
        }
        cropLabels[0].backgroundColor = .white
        cropLabels[0].setTitleColor(.black, for: .normal)
       
    }
    
    @IBAction func crop2Pressed(_ sender: UIButton) {
        controller.resetCropRect()
        controller.cropAspectRatio = 16 / 9
        controller.isExpandable = false
        
        cropLabels.forEach{$0.backgroundColor = .clear
            $0.setTitleColor(.white, for: .normal)
        }
        cropLabels[1].backgroundColor = .white
        cropLabels[1].setTitleColor(.black, for: .normal)
       
    }
    
    @IBAction func cropFreePressed(_ sender: UIButton) {

        loader.stopAnimating()
        controller.resetCropRect()
        controller.originalCropRect()
        controller.isExpandable = true
        
        cropLabels.forEach{$0.backgroundColor = .clear
            $0.setTitleColor(.white, for: .normal)
        }
        cropLabels[2].backgroundColor = .white
        cropLabels[2].setTitleColor(.black, for: .normal)
      
    }
    
    @IBAction func cropPressed(_ sender: UIButton) {
        if let image = controller.cropView?.croppedImage {
        
            controller.image = image
            ImageContainer.shared.modifiedImage = image
        }
        
    }
    
/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
