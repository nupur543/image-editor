//  RotateViewController.swift
//  ImageEditor
//  Created by Nupur Sharma on 25/01/19.
//  Copyright © 2019 Virtual Employee. All rights reserved.

import UIKit

class RotateViewController: UIViewController {

    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var imgView: UIImageView!
    
    var inputPhoto: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        self.setupBarButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       
        let image = ImageContainer.shared.modifiedImage
        imgView.image    = image
        inputPhoto       = image
        slider.value     = Float(ImageContainer.shared.imageOrientation)
      
    }
    
    // MARK: Setup - ⚙️
    fileprivate func setupBarButton() {
        let undo = UIBarButtonItem(title: "Reset", style: .done, target: self, action: #selector(undoPressed))
        self.parent?.navigationItem.leftBarButtonItems = [UIBarButtonItem(image: UIImage(named: "ic_back"),
                                                                          style: .plain,
                                                                          target: self,
                                                                          action: #selector(backPressed)), undo]
        
        self.parent?.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_tick"),
                                                                         style: .done,
                                                                         target: self,
                                                                         action: #selector(savePressed))
    }
    
    @objc func backPressed(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func savePressed(){
        
        self.performSegue(withIdentifier: "showEdittedSegue", sender: self)
    }
    
    @objc func undoPressed(){
        slider.value = 0.0
        sliderValueChanged(slider)
        ImageContainer.shared.imageOrientation = 0.0
    }
    
     func rotateImage(){
        
        let degrees: CGFloat = CGFloat(slider.value)
        let radians = CGFloat(__sinpi(degrees.native/180.0))
        self.imgView.transform = CGAffineTransform(rotationAngle: 0)
        
        inputPhoto = inputPhoto.rotate(radians: CGFloat(radians))
        ImageContainer.shared.modifiedImage = inputPhoto

    }
    
    
    @IBAction func directRotatePressed(_ sender: UIButton){
     
        if let val = sender.titleLabel?.text?.replacingOccurrences(of: "°", with: ""){
            slider.setValue(Float(val)!, animated: true)
           self.sliderValueChanged(slider)
        }
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        imgView.translatesAutoresizingMaskIntoConstraints = false

        
        let angle = slider.value * 2 * .pi
             
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.25) {
                self.imgView.transform = CGAffineTransform(rotationAngle: CGFloat(angle / self.slider.maximumValue))
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
      
        ImageContainer.shared.imageOrientation = CGFloat(slider.value)
    }
  
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if segue.identifier == "showEdittedSegue"{
             ImageContainer.shared.imageOrientation = CGFloat(slider.value)
        }
    }
 
}
