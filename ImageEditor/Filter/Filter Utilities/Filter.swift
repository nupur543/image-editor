//
//  Filter.swift
//  ImageEditor
//
//  Created by Nupur Sharma on 28/01/19.
//  Copyright © 2019 Virtual Employee. All rights reserved.
//

import UIKit

class Filter: NSObject {
    var name: String
    var displayName: String
    var isComposite: Bool = false
    var properties: [FilterProperty]?

    
    init(_ name: String, displayName: String, properties: [FilterProperty]?,isComposite: Bool ) {
        self.properties = [FilterProperty]()
        
        self.name = name
        self.displayName = displayName
        self.properties = properties
        self.isComposite = isComposite
        
    }
    
    func apply(_ image: UIImage) -> UIImage? {
        if self.isComposite {
            return applyComposite(image)
        }

        if let filter = CIFilter(name: self.name) {
          
            filter.setValue(image.toCIImage(), forKey: kCIInputImageKey)
            
            if let properties = self.properties{
            for property in properties{
            
                if filter.inputKeys.contains(property.name){
                    filter.setValue(property.value, forKey: property.name)
                }
              }
            }
            if let uiImg = filter.outputImage?.toUIImage(){
                return uiImg
            }
            return nil
        }
        return nil
    }
    
    
    func applyComposite(_ image: UIImage) -> UIImage? {
        
        let ciImg = image.toCIImage()!
        switch self.displayName {
        case "Old":
           return self.oldPhoto(img: ciImg).toUIImage()
        case "Nashville":
            return nashvilleFilter(foregroundImage: ciImg)!.toUIImage()
        case "Toaster":
            return toasterFilter(ciImage: ciImg)!.toUIImage()
        case "1977":
            return apply1977Filter(ciImage: ciImg)!.toUIImage()
        case "Clarendon":
            return clarendonFilter(foregroundImage: ciImg)!.toUIImage()
        case "HazeRemoval":
            return hazeRemovalFilter(image: ciImg)!.toUIImage()
        default:
            return image
        }
    }
}

struct FilterProperty {
    var name: String
    var displayName: String
    var value: Float?
}

extension Filter {
    
     var newIntensity: Float {
        if let properties = self.properties{
            for property in properties{
                if property.name == kCIInputIntensityKey{
                     return property.value!
                }
            }
        }
        
       return 0
    }
  
    var newSaturation: Float {
       
        if let properties = self.properties{
            for property in properties{
                if property.name == kCIInputSaturationKey{
                    return property.value!
                }
            }
        }
        return 0
    }
    
    var newBrightness: Float {
        if let properties = self.properties{
            for property in properties{
                if property.name == kCIInputBrightnessKey{
                    return property.value!
                }
            }
        }
        return 0
    }
    
    var newContrast: Float {
        if let properties = self.properties{
            for property in properties{
                if property.name == kCIInputContrastKey{
                    return property.value!
                }
            }
        }
        
        return 0
    }
    
   private func oldPhoto(img: CIImage) -> CIImage {
        
        let sepia = CIFilter(name:"CISepiaTone")
        sepia!.setValue(img, forKey:kCIInputImageKey)
        sepia!.setValue(newIntensity, forKey:"inputIntensity")
      
        let random = CIFilter(name:"CIRandomGenerator")
     
        let lighten = CIFilter(name:"CIColorControls")
        lighten!.setValue(random!.outputImage, forKey:kCIInputImageKey)
        lighten!.setValue(1 - newIntensity, forKey:"inputBrightness")
        lighten!.setValue(0, forKey:"inputSaturation")
      
        let croppedImage = lighten!.outputImage?.cropped(to: img.extent)
  
        let composite = CIFilter(name:"CIHardLightBlendMode")
        composite!.setValue(sepia!.outputImage, forKey:kCIInputImageKey)
        composite?.setValue(croppedImage, forKey:kCIInputBackgroundImageKey)
      
        let vignette = CIFilter(name:"CIVignette")
        vignette!.setValue(composite!.outputImage, forKey:kCIInputImageKey)
        vignette!.setValue(newIntensity * 2, forKey:"inputIntensity")
        vignette!.setValue(newIntensity * 30, forKey:"inputRadius")
    
        return vignette!.outputImage!
    }
    
        private func clarendonFilter(foregroundImage: CIImage) -> CIImage? {
            let backgroundImage = Filter.getColorImage(red: 127, green: 187, blue: 227, alpha: Int(255 * 0.2), rect: foregroundImage.extent)
            return foregroundImage.applyingFilter("CIOverlayBlendMode", parameters: [
                "inputBackgroundImage": backgroundImage,
                ])
                .applyingFilter("CIColorControls", parameters: [
                    "inputSaturation": newSaturation * 2.7,//1.35,
                    "inputBrightness": newBrightness * 0.1,//0/05
                    "inputContrast": newContrast * 2.2//1.1,
                    ])
        }
        
       private func nashvilleFilter(foregroundImage: CIImage) -> CIImage? {
            let backgroundImage = Filter.getColorImage(red: 247, green: 176, blue: 153, alpha: Int(255 * 0.56), rect: foregroundImage.extent)
            let backgroundImage2 = Filter.getColorImage(red: 0, green: 70, blue: 150, alpha: Int(255 * 0.4), rect: foregroundImage.extent)
            return foregroundImage
                .applyingFilter("CIDarkenBlendMode", parameters: [
                    "inputBackgroundImage": backgroundImage,
                    ])
                .applyingFilter("CISepiaTone", parameters: [
                    "inputIntensity": 0.2,
                    ])
                .applyingFilter("CIColorControls", parameters: [
                    "inputSaturation": newSaturation * 2.4,//1.2,
                    "inputBrightness": newBrightness * 0.1,
                    "inputContrast": newContrast * 2.2,
                    ])
                .applyingFilter("CILightenBlendMode", parameters: [
                    "inputBackgroundImage": backgroundImage2,
                    ])
        }
        
      private func apply1977Filter(ciImage: CIImage) -> CIImage? {
            let filterImage = Filter.getColorImage(red: 243, green: 106, blue: 188, alpha: Int(255 * 0.1), rect: ciImage.extent)
            let backgroundImage = ciImage
                .applyingFilter("CIColorControls", parameters: [
                    "inputSaturation": newSaturation * 2.6,//1.3,
                    "inputBrightness": newBrightness * 0.02,//0.1,
                    "inputContrast": newContrast * 2.1//1.05,
                    ])
                .applyingFilter("CIHueAdjust", parameters: [
                    "inputAngle": 0.3,
                    ])
            return filterImage
                .applyingFilter("CIScreenBlendMode", parameters: [
                    "inputBackgroundImage": backgroundImage,
                    ])
                .applyingFilter("CIToneCurve", parameters: [
                    "inputPoint0": CIVector(x: 0, y: 0),
                    "inputPoint1": CIVector(x: 0.25, y: 0.20),
                    "inputPoint2": CIVector(x: 0.5, y: 0.5),
                    "inputPoint3": CIVector(x: 0.75, y: 0.80),
                    "inputPoint4": CIVector(x: 1, y: 1),
                    ])
        }
        
       private func toasterFilter(ciImage: CIImage) -> CIImage? {
            let width = ciImage.extent.width
            let height = ciImage.extent.height
            let centerWidth = width / 2.0
            let centerHeight = height / 2.0
            let radius0 = min(width / 4.0, height / 4.0)
            let radius1 = min(width / 1.5, height / 1.5)
            
        let color0 = Filter.getColor(red: 128, green: 78, blue: 15, alpha: 255)
            let color1 = Filter.getColor(red: 79, green: 0, blue: 79, alpha: 255)
            let circle = CIFilter(name: "CIRadialGradient", parameters: [
                "inputCenter": CIVector(x: centerWidth, y: centerHeight),
                "inputRadius0": radius0,
                "inputRadius1": radius1,
                "inputColor0": color0,
                "inputColor1": color1,
                ])?.outputImage?.cropped(to: ciImage.extent)
            
            return ciImage
                .applyingFilter("CIColorControls", parameters: [
                    "inputSaturation": newSaturation * 0.5,//1.0,
                    "inputBrightness": newBrightness * 0.02,//0.01,
                    "inputContrast": newContrast * 2.2
                    ])
                .applyingFilter("CIScreenBlendMode", parameters: [
                    "inputBackgroundImage": circle!,
                    ])
        }
        
        
      private func hazeRemovalFilter(image: CIImage) -> CIImage? {
            let filter = HazeRemovalFilter()
            filter.inputImage = image
            return filter.outputImage
        }
        
        private static func getColor(red: Int, green: Int, blue: Int, alpha: Int = 255) -> CIColor {
            return CIColor(red: CGFloat(Double(red) / 255.0),
                           green: CGFloat(Double(green) / 255.0),
                           blue: CGFloat(Double(blue) / 255.0),
                           alpha: CGFloat(Double(alpha) / 255.0))
        }
        
        private static func getColorImage(red: Int, green: Int, blue: Int, alpha: Int = 255, rect: CGRect) -> CIImage {
            let color = self.getColor(red: red, green: green, blue: blue, alpha: alpha)
            return CIImage(color: color).cropped(to: rect)
        }
    }
    
    class HazeRemovalFilter: CIFilter {
        
        var inputImage: CIImage!
        var inputColor: CIColor! = CIColor(red: 0.7, green: 0.9, blue: 1.0)
        var inputDistance: Float! = 0.2
        var inputSlope: Float! = 0.0
        var hazeRemovalKernel: CIKernel!
        
        override init()
        {
            // check kernel has been already initialized
            let code: String = """
kernel vec4 myHazeRemovalKernel(
    sampler src,
    __color color,
    float distance,
    float slope)
{
    vec4 t;
    float d;

    d = destCoord().y * slope + distance;
    t = unpremultiply(sample(src, samplerCoord(src)));
    t = (t - d * color) / (1.0 - d);

    return premultiply(t);
}
"""
            self.hazeRemovalKernel = CIKernel(source: code)
            super.init()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override var outputImage: CIImage?
        {
            guard let inputImage = self.inputImage,
                let hazeRemovalKernel = self.hazeRemovalKernel,
                let inputColor = self.inputColor,
                let inputDistance = self.inputDistance,
                let inputSlope = self.inputSlope
                else {
                    return nil
            }
            let src: CISampler = CISampler(image: inputImage)
            return hazeRemovalKernel.apply(extent: inputImage.extent,
                                           roiCallback: { (index, rect) -> CGRect in
                                            return rect
            }, arguments: [
                src,
                inputColor,
                inputDistance,
                inputSlope,
                ])
        }
        
        override var attributes: [String : Any] {
            return [
                kCIAttributeFilterDisplayName: "Haze Removal Filter",
                "inputDistance": [
                    kCIAttributeMin: 0.0,
                    kCIAttributeMax: 1.0,
                    kCIAttributeSliderMin: 0.0,
                    kCIAttributeSliderMax: 0.7,
                    kCIAttributeDefault: 0.2,
                    kCIAttributeIdentity : 0.0,
                    kCIAttributeType: kCIAttributeTypeScalar
                ],
                "inputSlope": [
                    kCIAttributeSliderMin: -0.01,
                    kCIAttributeSliderMax: 0.01,
                    kCIAttributeDefault: 0.00,
                    kCIAttributeIdentity: 0.00,
                    kCIAttributeType: kCIAttributeTypeScalar
                ],
                kCIInputColorKey: [
                    kCIAttributeDefault: CIColor(red:1.0, green:1.0, blue:1.0, alpha:1.0)
                ],
            ]
        }
    }
