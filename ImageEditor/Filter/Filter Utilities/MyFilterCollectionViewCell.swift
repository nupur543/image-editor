//
//  MyFilterCollectionViewCell.swift
//  ImageEditor
//
//  Created by Nupur Sharma on 21/01/19.
//  Copyright © 2019 Virtual Employee. All rights reserved.
//

import UIKit

class MyFilterCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var properties: [FilterProperty]?
    
    override var isSelected: Bool { didSet {
        name.textColor = isSelected
            ? UIColor.darkGray
            : UIColor.white
        
        name.backgroundColor = isSelected
            ? UIColor.white
            : UIColor.clear
        
        name.font = .systemFont(ofSize: 14, weight: isSelected
            ? UIFont.Weight.medium
            : UIFont.Weight.regular)
        }
    }
    
    override func awakeFromNib() {
        imageView.clipsToBounds = true

        self.clipsToBounds = false
        self.layer.shadowColor = UIColor(red: 46, green: 43, blue: 37, alpha: 1.0).cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 4, height: 7)
        self.layer.shadowRadius = 5
        self.layer.backgroundColor = UIColor.clear.cgColor
    }
}
