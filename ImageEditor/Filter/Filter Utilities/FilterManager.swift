//
//  FilterManager.swift
//  ImageEditor
//
//  Created by Nupur Sharma on 28/01/19.
//  Copyright © 2019 Virtual Employee. All rights reserved.
//

import UIKit

class FilterManager: NSObject {
    static let shared = FilterManager()
    private override init() {
        super.init()
        
        prepareFilters()
    }

    var items = [Filter]()
   
    private func prepareFilters() {
      
        let properties = [FilterProperty(name: kCIInputIntensityKey, displayName: "Intensity", value: 1.0)]
        let properties2 = [FilterProperty(name: kCIInputSaturationKey, displayName: "Saturation", value: 0.5),
                           FilterProperty(name: kCIInputBrightnessKey, displayName: "Brightness", value: 0.5),
                           FilterProperty(name: kCIInputContrastKey, displayName: "Contrast", value: 0.5)]
        
        let filter = Filter("", displayName: "Original", properties: nil, isComposite: false)
        items.append(filter)
        
        let filter1 = Filter("CIPhotoEffectChrome", displayName: "Chrome", properties: nil, isComposite: false)
        items.append(filter1)
        
        let filter2 = Filter("CIPhotoEffectFade", displayName: "Fade", properties: nil, isComposite: false)
        items.append(filter2)
        
        
        let filter3 = Filter("CISepiaTone", displayName: "Sepia", properties: properties, isComposite: false)
        items.append(filter3)
        
        let filter4 = Filter("CIPhotoEffectTransfer", displayName: "Transfer", properties: nil, isComposite: false)
        items.append(filter4)
        
        let filter5 = Filter("CIPhotoEffectInstant", displayName: "Instant", properties: nil, isComposite: false)
        items.append(filter5)
        
        let filter6 = Filter("CIPhotoEffectTonal", displayName: "Tone", properties: nil, isComposite: false)
        items.append(filter6)
        
        let filter7 = Filter("Old", displayName: "Old", properties: properties, isComposite: true)
        items.append(filter7)
        
        let filter8 = Filter("Nashville", displayName: "Nashville", properties: properties2, isComposite: true)
        items.append(filter8)
        
        let filter9 = Filter("Toaster", displayName: "Toaster", properties: properties2, isComposite: true)
        items.append(filter9)
        
        let filter10 = Filter("1977", displayName: "1977", properties: properties2, isComposite: true)
        items.append(filter10)
        
        let filter11 = Filter("Clarendon", displayName: "Clarendon", properties: properties2, isComposite: true)
        items.append(filter11)
        
        let filter12 = Filter("HazeRemoval", displayName: "HazeRemoval", properties: nil, isComposite: true)
        items.append(filter12)
        
    }
    
}
