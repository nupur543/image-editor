//
//  PhotoFiltersVC.swift
//  photoTaking
//
//  Created by Sacha Durand Saint Omer on 21/10/16.
//  Copyright © 2016 octopepper. All rights reserved.
//

import UIKit

 class PhotoFiltersVC: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var collectnView: UICollectionView!
    @IBOutlet weak var tblViewInput: UITableView!
    @IBOutlet weak var tblViewHeight: NSLayoutConstraint!
    
    fileprivate let filterItems : [Filter] = FilterManager.shared.items

    var selectedIndexPath: IndexPath?
    
    fileprivate var inputIntensity: CGFloat?
    
    fileprivate var filteredThumbnailImagesArray: [UIImage] = []
    fileprivate var currentlySelectedImageThumbnail: UIImage? // Used for comparing with original image when tapped
    
    let imageProcessQueue = DispatchQueue.global(qos: .userInitiated)

    override open func viewDidLoad() {
        super.viewDidLoad()

        // Touch preview to see original image.
        let touchDownGR = UILongPressGestureRecognizer(target: self,
                                                       action: #selector(handleTouchDown))
        touchDownGR.minimumPressDuration = 0
        touchDownGR.delegate = self
        imgView.addGestureRecognizer(touchDownGR)
        imgView.isUserInteractionEnabled = true
        
      //  imgView.backgroundColor = .blue
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.loader.startAnimating()
        collectnView.isHidden = true
        tblViewHeight.constant = 0.0
        
        let image = ImageContainer.shared.modifiedImage
        self.imgView.image = image
    
        let angle = (ImageContainer.shared.imageOrientation) * 2 * .pi
        self.imgView.transform = CGAffineTransform(rotationAngle: angle / 90)

        DispatchQueue.global().async {
          self.filteredThumbnailImagesArray = self.filterItems.map { filter -> UIImage in
                if let outputImage = filter.apply((ImageContainer.shared.thumbModifiedImage))
                {
                    return outputImage
                } else {
                    return ImageContainer.shared.thumbModifiedImage
                }
            }
            
            DispatchQueue.main.async {
            
                self.collectnView.isHidden = false
                self.collectnView.reloadData()
                self.collectnView.selectItem(at: IndexPath(row: 0, section: 0),
                                             animated: false,
                                             scrollPosition: UICollectionView.ScrollPosition.bottom)
                self.loader.stopAnimating()
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let undo = UIBarButtonItem(title: "Reset", style: .done, target: self, action: #selector(undoPressed))
        self.parent?.navigationItem.leftBarButtonItems = [UIBarButtonItem(image: UIImage(named: "ic_back"),
                                                                          style: .plain,
                                                                          target: self,
                                                                          action: #selector(cancel)), undo]
        setupBarButton()
    
    }
    
 
    // MARK: Setup - ⚙️
   fileprivate func setupBarButton() {

    self.parent?.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_tick"),
                                                            style: .done,
                                                            target: self,
                                                            action: #selector(undoPressed))
    
    self.parent?.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_tick"),
                                                                     style: .done,
                                                                     target: self,
                                                                     action: #selector(savePressed))
    }
    
    // MARK: - Methods 🏓

    @objc
    fileprivate func handleTouchDown(sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
           imgView.image = ImageContainer.shared.thumbImage
        case .ended:
            imgView.image = currentlySelectedImageThumbnail ?? ImageContainer.shared.thumbModifiedImage
        default: ()
        }
    }
    
    fileprivate func thumbFromImage(_ img: UIImage) -> CIImage {
        let k = img.size.width / img.size.height
        let scale = UIScreen.main.scale
        let thumbnailHeight: CGFloat = 300 * scale
        let thumbnailWidth = thumbnailHeight * k
        let thumbnailSize = CGSize(width: thumbnailWidth, height: thumbnailHeight)
        UIGraphicsBeginImageContext(thumbnailSize)
        img.draw(in: CGRect(x: 0, y: 0, width: thumbnailSize.width, height: thumbnailSize.height))
        let smallImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return smallImage!.toCIImage()!
    }
    
    // MARK: - Actions 🥂

    @objc
    func cancel() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc
    func undoPressed() {
     
          self.collectnView.selectItem(at: IndexPath(item: 0, section: 0),
                                         animated: false,
                                         scrollPosition: .left)
            self.imgView.image = self.filteredThumbnailImagesArray[0]
            self.currentlySelectedImageThumbnail = self.filteredThumbnailImagesArray[0]
            self.selectedIndexPath = IndexPath(row: 0, section: 0)
            self.tblViewInput.reloadData()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let num = selectedIndexPath?.row{
            let filter = filterItems[num]
            let newImg = filter.apply(ImageContainer.shared.originalImage.fixOrientation())
            ImageContainer.shared.modifiedImage = newImg
           
        }else{
            ImageContainer.shared.modifiedImage  =  ImageContainer.shared.originalImage
           
        }
        
    }
    
    @objc func savePressed(){
        
        self.performSegue(withIdentifier: "showEdittedSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showEdittedSegue"{
            if let num = selectedIndexPath?.row{
                let filter = filterItems[num]
                let newImg = filter.apply(ImageContainer.shared.originalImage.fixOrientation())
                ImageContainer.shared.modifiedImage = newImg
                
            }else{
                ImageContainer.shared.modifiedImage  =  ImageContainer.shared.originalImage
                
            }
        }
    }

}

extension PhotoFiltersVC: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return filteredThumbnailImagesArray.count
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCell",for: indexPath) as! MyFilterCollectionViewCell
        
        let filter = filterItems[indexPath.row]
         let image = filteredThumbnailImagesArray[indexPath.row]
        cell.name.text       = filter.displayName
        cell.imageView.image = image
        return cell
    }
}

extension PhotoFiltersVC: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let filter = filterItems[indexPath.row]
        selectedIndexPath = indexPath
        
        currentlySelectedImageThumbnail = filteredThumbnailImagesArray[indexPath.row]
        imgView.image = currentlySelectedImageThumbnail
       
        collectnView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated:true)
        DispatchQueue.main.async {
          self.tblViewInput.reloadData()
        }
    }
}

extension PhotoFiltersVC: UITableViewDelegate, UITableViewDataSource{
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let indexPath = selectedIndexPath {
            let filter = filterItems[indexPath.row]
            tblViewHeight.constant = CGFloat(60 * (filter.properties?.count ?? 0))
            return filter.properties?.count ?? 0
        }
        
        return 0
      
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "inputCellID", for: indexPath) as! FilterInputCell
        
        if let indexPath2 = selectedIndexPath {
            let filter = filterItems[indexPath2.row]
            if let pps = filter.properties{
                let property = pps[indexPath.row]
                cell.lblInput.text = property.displayName
                cell.valueSlider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
            }
            
        }
 
        return cell
    }
    
    @objc func sliderValueChanged(_ sender: UISlider){
    
        guard let cell = sender.superview?.superview as? FilterInputCell else {
            return
        }
        
        guard let indexPath = tblViewInput.indexPath(for: cell) else{
            return
        }
        
        imageProcessQueue.async {
            let filter = self.filterItems[(self.selectedIndexPath?.row)!]
            var property = filter.properties?[indexPath.row]
            
            DispatchQueue.main.async {
                property!.value =  sender.value
                filter.properties?[indexPath.row] = property!
                if let newImg = filter.apply(ImageContainer.shared.thumbModifiedImage){
                    self.imgView.image = newImg
                }
            }
        }
    }
}

class FilterInputCell: UITableViewCell{
    //inputCellID
    @IBOutlet weak var lblInput: UILabel!
    @IBOutlet weak var valueSlider: UISlider!
    
    override func awakeFromNib() {
       // print("awakeFromNib")
       
    }
    
    override func prepareForReuse() {
        //print("prepareForReuse")
        valueSlider.minimumValue = 0
        valueSlider.maximumValue = 2.0
        valueSlider.value = 1.0
    }
}
